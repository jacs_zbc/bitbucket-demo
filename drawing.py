###################################################################
# EKSEMPEL: main.py for Jørgen, Lise og Tom kunne fx se således ud
###################################################################

import turtle
bob = turtle.Turtle()
lise = turtle.Turtle()

def square(t,length):
    ''' Draw a square
    Input:  t - Turtle
            length - (int) Sidelength
    '''
    return length*2


def random_pos(x1,y1,x2,y2):
    ''' Return a random 2D position inside [x1; x2] and [y1; y2]
    Input:  x1, y1  - (int) First corner
            x2, y2  - (int) Second corner
    Output: x,y     - (int) Random position
    '''
    msg = "Streng"

# TODO - Tom: Hovedløkken til at bruge den tilfældige position og tegne 10 firkanter